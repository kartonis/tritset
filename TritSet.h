#pragma ones
#include <iostream>
#include <unordered_map>
typedef unsigned int uint;
enum Trit { False, Unknown, True };
class OneTrit;
class TritSet
{
    friend class OneTrit;
    uint *pointer;
    uint len = 0;
    uint intlen = 0;
    uint loglen = 0;
public:
    TritSet() { }
    TritSet(uint n);
    TritSet(const TritSet &ob);
    ~TritSet() { delete[] pointer; };
    size_t capacity() const { return len; }
    void shrink();
    TritSet operator&(const TritSet &ob) const;
    TritSet operator|(const TritSet &ob) const;
    TritSet operator!() const;
    OneTrit operator[](uint n);
    Trit operator[](uint n) const;
    TritSet &operator=(const TritSet &ob);
    size_t cardinality(Trit value) const;
    std::unordered_map< Trit, int, std::hash<int> > cardinality() const;
    void trim(uint lastIndex);
    size_t length()const { return loglen; }
};
uint createUnknown();
class OneTrit
{
    friend class TritSet;
    uint intAdress;
    uint tritAdress;
    TritSet &obj;
    void memory();
    OneTrit(uint _intAdress, uint _tritAdress, TritSet &ob) : intAdress(_intAdress), tritAdress(_tritAdress), obj(ob) {}
public:
    OneTrit &operator = (OneTrit &value);
    OneTrit &operator = (Trit value);
    operator Trit() const;
};

