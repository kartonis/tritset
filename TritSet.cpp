#include "TritSet.h"
TritSet::TritSet(uint n)
{
    uint byteUnknown = 0;
    len = n;
    intlen = n / (8 * sizeof(uint) / 2) + ((n % (8 * sizeof(uint) / 2)) ? 1 : 0);
    if (intlen > 0)
        pointer = new uint[intlen];
    byteUnknown = createUnknown();
    for (uint i = 0; i < intlen; ++i)
        pointer[i] = byteUnknown;
}
TritSet::TritSet(const TritSet &ob)
{
    len = ob.len;
    intlen = ob.intlen;
    loglen = ob.loglen;
    if (intlen > 0)
    {
        pointer = new uint[intlen];
        for (uint i = 0; i < intlen; ++i)
            pointer[i] = ob.pointer[i];
    }
}
void TritSet::shrink()
{
    uint logintlen = loglen / (8 * sizeof(uint) / 2) + ((loglen % (8 * sizeof(uint) / 2)) ? 1 : 0);
    if (logintlen < intlen)
    {
        uint *copy = new uint[logintlen];
        for (uint i = 0; i < logintlen; ++i)
            copy[i] = pointer[i];
        delete[] pointer;
        pointer = copy;
        intlen = logintlen;
    }
    len = loglen;
}
OneTrit TritSet::operator[](uint i)
{
    uint intAdress = i / (4 * sizeof(uint));
    uint tritAdress = i % (4 * sizeof(uint));
    OneTrit obj(intAdress, tritAdress, *this);
    return obj;
}
OneTrit::operator Trit() const
{
    if (intAdress >= obj.intlen)
        return Unknown;
    uint tmp = obj.pointer[intAdress];
    for (int i = sizeof(uint) * 4 - 1; i > tritAdress; --i)
        tmp >>= 2;
    tmp &= 3;
    switch (tmp) {
        case 0:
            return False;
        case 1:
            return Unknown;
        case 2:
            return True;
        default:
            exit(0);
    }
}
uint createUnknown()
{
    uint arrayUnknown = 0;
    for (int i = 0; i < sizeof(uint) * 8 / 2; ++i)
    {
        arrayUnknown <<= 2;
        arrayUnknown |= 1;
    }
    return arrayUnknown;
}
void OneTrit::memory()
{
    if (intAdress >= obj.intlen)
    {
        uint arrayUnknown = createUnknown();
        if (obj.intlen == 0)
        {
            obj.pointer = new uint[intAdress + 1];
            obj.intlen = intAdress + 1;
            for (int i = 0; i < obj.intlen; ++i)
                obj.pointer[i] = arrayUnknown;
        }
        else
        {
            uint *newPointer = new uint[intAdress + 1];
            for (int i = 0; i < obj.intlen; ++i)
                newPointer[i] = obj.pointer[i];
            delete[] obj.pointer;
            obj.pointer = newPointer;
            for (int i = obj.intlen; i < intAdress + 1; ++i)
                obj.pointer[i] = arrayUnknown;
            obj.intlen = intAdress + 1;
        }
    }
}
OneTrit &OneTrit::operator=(Trit value)
{
    uint mask = 0;
    uint mask_two = 0;
    if (value == False)
    {
        for (int i = 0; i < sizeof(uint) * 8; ++i)
        {
            mask <<= 1;
            if (i != tritAdress * 2 && i != tritAdress * 2 + 1)
                mask |= 1;
        }
        memory();
        obj.pointer[intAdress] &= mask;
        if (intAdress * sizeof(uint) * 4 + tritAdress + 1 > obj.loglen)
        {
            obj.loglen = intAdress * sizeof(uint) * 4 + tritAdress + 1;
            if (obj.len < obj.loglen) obj.len = obj.loglen;
        }
    }
    if (value == Unknown && intAdress < obj.intlen)
    {
        for (int i = 0; i < sizeof(uint) * 8; ++i)
        {
            mask <<= 1;
            mask_two <<= 1;
            if (i != tritAdress * 2)
                mask |= 1;
            if (i == tritAdress * 2 + 1)
                mask_two |= 1;
        }
        obj.pointer[intAdress] &= mask;
        obj.pointer[intAdress] |= mask_two;
        if (intAdress * sizeof(uint) * 4 + tritAdress + 1 == obj.loglen)
        {
            uint i = obj.loglen - 1;
            while (obj[i] == Unknown)
                i--;
            obj.loglen = i + 1;
        }
    }
    if (value == True)
    {
        for (int i = 0; i < sizeof(uint) * 8; ++i)
        {
            mask <<= 1;
            mask_two <<= 1;
            if (i == tritAdress * 2)
                mask |= 1;
            if (i != tritAdress * 2 + 1)
                mask_two |= 1;
        }
        memory();
        obj.pointer[intAdress] |= mask;
        obj.pointer[intAdress] &= mask_two;
        if (intAdress * sizeof(uint) * 4 + tritAdress + 1 > obj.loglen)
        {
            obj.loglen = intAdress * sizeof(uint) * 4 + tritAdress + 1;
            if (obj.len < obj.loglen) obj.len = obj.loglen;
        }
    }
    return *this;
}
OneTrit &OneTrit::operator=(OneTrit &value)
{
    uint mask = 0;
    uint mask_two = 0;
    if (value == False)
    {
        for (int i = 0; i < sizeof(uint) * 8; ++i)
        {
            mask <<= 1;
            if (i != tritAdress * 2 && i != tritAdress * 2 + 1)
                mask |= 1;
        }
        memory();
        obj.pointer[intAdress] &= mask;
        if (intAdress * sizeof(uint) * 4 + tritAdress + 1 > obj.loglen)
        {
            obj.loglen = intAdress * sizeof(uint) * 4 + tritAdress + 1;
            if (obj.len < obj.loglen) obj.len = obj.loglen;
        }
    }
    if (value == Unknown && intAdress < obj.intlen)
    {
        for (int i = 0; i < sizeof(uint) * 8; ++i)
        {
            mask <<= 1;
            mask_two <<= 1;
            if (i != tritAdress * 2)
                mask |= 1;
            if (i == tritAdress * 2 + 1)
                mask_two |= 1;
        }
        obj.pointer[intAdress] &= mask;
        obj.pointer[intAdress] |= mask_two;
        if (intAdress * sizeof(uint) * 4 + tritAdress + 1 == obj.loglen)
        {
            uint i = obj.loglen - 1;
            while (obj[i] == Unknown)
                i--;
            obj.loglen = i + 1;
        }
    }
    if (value == True)
    {
        for (int i = 0; i < sizeof(uint) * 8; ++i)
        {
            mask <<= 1;
            mask_two <<= 1;
            if (i == tritAdress * 2)
                mask |= 1;
            if (i != tritAdress * 2 + 1)
                mask_two |= 1;
        }
        memory();
        obj.pointer[intAdress] |= mask;
        obj.pointer[intAdress] &= mask_two;
        if (intAdress * sizeof(uint) * 4 + tritAdress + 1 > obj.loglen)
        {
            obj.loglen = intAdress * sizeof(uint) * 4 + tritAdress + 1;
            if (obj.len < obj.loglen) obj.len = obj.loglen;
        }
    }
    return *this;
}
Trit TritSet::operator[](uint n) const
{
    uint intAdress = n / (4 * sizeof(uint));
    uint tritAdress = n % (4 * sizeof(uint));
    if (intAdress >= intlen)
        return Unknown;
    uint tmp = pointer[intAdress];
    for (int i = sizeof(uint) * 4 - 1; i > tritAdress; --i)
        tmp >>= 2;
    tmp &= 3;
    switch (tmp) {
        case 0:
            return False;
        case 1:
            return Unknown;
        case 2:
            return True;
        default:
            exit(0);
    }
}
TritSet TritSet::operator&(const TritSet &ob) const
{
    TritSet res = ((this->loglen > ob.loglen) ? *this : ob);
    for (int i = 0; i < res.loglen; ++i)
    {
        if ((*this)[i] == False || ob[i] == False) res[i] = False;
        else if ((*this)[i] == True && ob[i] == True) res[i] = True;
        else res[i] = Unknown;
    }
    uint i = res.loglen;
    while (res[i] == Unknown)
        i--;
    res.loglen = i + 1;
    return res;
}
TritSet TritSet::operator|(const TritSet &ob) const
{
    TritSet res = ((this->loglen > ob.loglen) ? *this : ob);
    for (int i = 0; i < res.loglen; ++i)
    {
        if ((*this)[i] == False && ob[i] == False) res[i] = False;
        else if ((*this)[i] == True || ob[i] == True) res[i] = True;
        else res[i] = Unknown;
    }
    uint i = res.loglen;
    while (res[i] == Unknown)
        i--;
    res.loglen = i + 1;
    return res;
}
TritSet TritSet::operator!() const
{
    TritSet res = (*this);
    for (int i = 0; i < res.loglen; ++i)
    {
        switch (res[i]) {
            case True:
                res[i] = False;
                break;
            case False:
                res[i] = True;
                break;
            default:
                res[i] = Unknown;
                break;
        }
    }
    return res;
}
TritSet &TritSet::operator=(const TritSet &ob)
{
    len = ob.len;
    intlen = ob.intlen;
    loglen = ob.loglen;
    if (intlen > 0)
    {
        pointer = new uint[intlen];
        for (uint i = 0; i < intlen; ++i)
            pointer[i] = ob.pointer[i];
    }
    return *this;
}
size_t TritSet::cardinality(Trit value) const
{
    size_t count = 0;
    for (int i = 0; i < this->loglen; ++i)
    {
        if ((*this)[i] == value) count++;
    }
    return count;
}
std::unordered_map< Trit, int, std::hash<int> > TritSet::cardinality() const
{
    std::unordered_map <Trit, int, std::hash<int>> myMap;
    uint count[3] = { 0 };
    for (int i = 0; i < loglen; ++i)
        count[(*this)[i]]++;
    for (int i = 0; i < 3; ++i)
        myMap.insert({ (Trit)i, count[i] });
    return myMap;
}
void TritSet::trim(uint lastIndex)
{
    for (uint i = lastIndex; i < loglen; ++i)
        (*this)[i] = Unknown;
    while ((*this)[lastIndex] == Unknown)
        lastIndex--;
    loglen = lastIndex + 1;
}

