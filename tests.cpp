#include <iostream>
#include "gtest/gtest.h"
#include "TritSet.h"

TEST(Memory_and_index_operator, Case1)
{
    TritSet ob;
    for (uint i = 0; i < 30; ++i)
        ob[i] = False;
    for (uint i = 0; i < 30; ++i)
        EXPECT_EQ(False, ob[i]);
}
TEST(Shrink, Case1)
{
    TritSet ob;
    for (uint i = 0; i < 100; ++i)
        ob[i] = True;
    for (uint i = 70; i < 100; ++i)
        ob[i] = Unknown;
    ob.shrink();
    for (uint i = 0; i < 70; ++i)
        EXPECT_EQ(True, ob[i]);
    for (uint i = 70; i < 100; ++i)
        EXPECT_EQ(Unknown, ob[i]);
}
TEST(Constructor, Case1)
{
    TritSet ob;
    for (uint i = 0; i < 40; ++i)
        ob[i] = Trit(i % 3);
    for (uint i = 0; i < 40; ++i)
        EXPECT_EQ(Trit(i % 3), ob[i]);
}
TEST(CopyConstructor, Case1)
{
    TritSet ob;
    for (int i = 0; i < 200; ++i)
        ob[i] = False;
    TritSet obcopy = ob;
    for (int i = 0; i < 200; ++i)
        ob[i] = True;
    for (int i = 0; i < 200; ++i)
        EXPECT_EQ(False, obcopy[i]);
    for (int i = 0; i < 200; ++i)
        EXPECT_EQ(True, ob[i]);
}
TEST(Trit_And_Operator, Case1)
{
    TritSet ob1, ob2, res;
    ob1[0] = False;
    ob2[0] = False;
    ob1[1] = False;
    ob2[1] = Unknown;
    ob1[2] = False;
    ob2[2] = True;
    ob1[3] = Unknown;
    ob2[3] = False;
    ob1[4] = Unknown;
    ob2[4] = Unknown;
    ob1[5] = Unknown;
    ob2[5] = True;
    ob1[6] = True;
    ob2[6] = False;
    ob1[7] = True;
    ob2[7] = Unknown;
    ob1[8] = True;
    ob2[8] = True;
    res = ob1 & ob2;
    EXPECT_EQ(False, res[0]);
    EXPECT_EQ(False, res[1]);
    EXPECT_EQ(False, res[2]);
    EXPECT_EQ(False, res[3]);
    EXPECT_EQ(Unknown, res[4]);
    EXPECT_EQ(Unknown, res[5]);
    EXPECT_EQ(False, res[6]);
    EXPECT_EQ(Unknown, res[7]);
    EXPECT_EQ(True, res[8]);
    EXPECT_EQ(9, res.length());
    EXPECT_EQ(9, res.capacity());
}
TEST(Trit_Or_Operator, Case1)
{
    TritSet ob1, ob2, res;
    ob1[0] = False;
    ob2[0] = False;
    ob1[1] = False;
    ob2[1] = Unknown;
    ob1[2] = False;
    ob2[2] = True;
    ob1[3] = Unknown;
    ob2[3] = False;
    ob1[4] = Unknown;
    ob2[4] = Unknown;
    ob1[5] = Unknown;
    ob2[5] = True;
    ob1[6] = True;
    ob2[6] = False;
    ob1[7] = True;
    ob2[7] = Unknown;
    ob1[8] = True;
    ob2[8] = True;
    res = ob1 | ob2;
    EXPECT_EQ(False, res[0]);
    EXPECT_EQ(Unknown, res[1]);
    EXPECT_EQ(True, res[2]);
    EXPECT_EQ(Unknown, res[3]);
    EXPECT_EQ(Unknown, res[4]);
    EXPECT_EQ(True, res[5]);
    EXPECT_EQ(True, res[6]);
    EXPECT_EQ(True, res[7]);
    EXPECT_EQ(True, res[8]);
    EXPECT_EQ(9, res.length());
    EXPECT_EQ(9, res.capacity());
}

TEST(Trit_Not_Operator, Case1)
{
    TritSet ob;
    ob[0] = False;
    ob[1] = Unknown;
    ob[2] = True;
    ob = !ob;
    EXPECT_EQ(True, ob[0]);
    EXPECT_EQ(Unknown, ob[1]);
    EXPECT_EQ(False, ob[2]);
    EXPECT_EQ(3, ob.length());
    EXPECT_EQ(3, ob.capacity());
}
TEST(JustNumberOfTrits, Case1)
{
    size_t countFalse = 0;
    size_t countUnknown = 0;
    size_t countTrue = 0;
    TritSet ob;
    ob[0] = False;
    ob[2] = False;
    ob[3] = False;
    ob[5] = False;
    ob[6] = False;
    ob[8] = False;
    ob[9] = True;
    ob[10] = True;
    ob[11] = True;
    ob[12] = True;
    ob[16] = True;
    countFalse = ob.cardinality(False);
    countUnknown = ob.cardinality(Unknown);
    countTrue = ob.cardinality(True);
    EXPECT_EQ(6, countFalse);
    EXPECT_EQ(6, countUnknown);
    EXPECT_EQ(5, countTrue);
}
TEST(MapNumberOfTrits, Case1)
{
    TritSet ob;
    ob[0] = False;
    ob[2] = False;
    ob[3] = False;
    ob[5] = False;
    ob[6] = False;
    ob[8] = False;
    ob[9] = True;
    ob[10] = True;
    ob[11] = True;
    ob[12] = True;
    ob[16] = True;
    std::unordered_map <Trit, int, std::hash<int> > myMap = ob.cardinality();
    EXPECT_EQ(5, myMap[True]);
    EXPECT_EQ(6, myMap[False]);
    EXPECT_EQ(6, myMap[Unknown]);
}
TEST(Trim, Case1)
{
    TritSet ob;
    ob[0] = True;
    ob[11] = False;
    ob.trim(4);
    EXPECT_EQ(Unknown, ob[11]);
    EXPECT_EQ(1, ob.length());
}
TEST(Trim, Case2)
{
    TritSet ob;
    ob[0] = True;
    ob[11] = False;
    ob.trim(12);
    EXPECT_EQ(False, ob[11]);
    EXPECT_EQ(12, ob.length());
}
TEST(CreateUnknown, Case1)
{
    uint onebyte = createUnknown();
    for (uint i = sizeof(uint) * 4; i > 0; --i)
    {
        EXPECT_EQ(Unknown, onebyte & 3);
        onebyte >>= 2;
    }
}
int main(int argc, char** argv)
{
    testing::InitGoogleTest(&argc, argv);
    RUN_ALL_TESTS();
    return 0;
}
